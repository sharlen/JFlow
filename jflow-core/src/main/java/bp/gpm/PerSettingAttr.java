package bp.gpm;

import bp.da.*;
import bp.en.*;
import java.util.*;

/** 
 个人设置
*/
public class PerSettingAttr extends EntityNoNameAttr
{
	/** 
	 系统
	*/
	public static final String FK_App = "FK_App";
	/** 
	 人员
	*/
	public static final String FK_Emp = "FK_Emp";
	/** 
	 UserNo
	*/
	public static final String UserNo = "UserNo";
	/** 
	 密码
	*/
	public static final String UserPass = "UserPass";
	/** 
	 Idx
	*/
	public static final String Idx = "Idx";
}